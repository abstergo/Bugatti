package test;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Administrator on 2017/12/12.
 */
public class consumer implements Runnable {

    BlockingQueue blockingQueue;

    public consumer(BlockingQueue blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        try {
            while(true)
            {
                System.out.println(Thread.currentThread().getName()+"--consumer:"+blockingQueue.take());
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
