package test;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created by Administrator on 2017/12/12.
 */
public class testMain {

    public static void main(String[] args) {
        BlockingQueue blockingQueue = new LinkedBlockingDeque(3);

        Producer producer = new Producer(blockingQueue);
        consumer consumer = new consumer(blockingQueue);

        Thread thread1 = new Thread(consumer);
        for (int i=0;i<20;i++) {
            Thread thread = new Thread(producer);
            thread.start();

        }
        thread1.start();
    }
}

