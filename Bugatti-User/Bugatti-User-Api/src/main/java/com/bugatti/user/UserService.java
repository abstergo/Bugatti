package com.bugatti.user;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Administrator on 2017/11/28.
 */
@FeignClient("bugatti-user")
public interface UserService {
    @RequestMapping(method = RequestMethod.GET, value = "/add")
    Integer add();
}
