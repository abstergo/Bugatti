package com.bugatti.controller;


import com.bugatti.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/11/28.
 */

@RestController
public class UserController implements Serializable{


    @Autowired
    UserService userService;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public Integer add() {
        return userService.add();
    }

}
